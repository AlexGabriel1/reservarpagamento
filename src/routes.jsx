import React from 'react';
import { Route, Switch, HashRouter } from 'react-router-dom';

import App from './pages/home/index';

const Routes = () => {
	return (
		<HashRouter>
			<Switch>
				<Route path="/:idveiculo">
					<App />
				</Route>
			</Switch>
		</HashRouter>
	);
};

export default Routes;
