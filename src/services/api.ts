import axios from 'axios';

const api = axios.create({
	baseURL: 'https://www.usadofacil.com.br/v6',
});

export default api;
