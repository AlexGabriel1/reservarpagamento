/* eslint-disable no-mixed-spaces-and-tabs */
import React, { useState } from 'react';
import NumberFormat from 'react-number-format';
import { useParams } from 'react-router-dom';

import { TextField, Button, Typography, Box, Stack, MenuItem, Select, CircularProgress } from '@material-ui/core';
import { Formik, Form } from 'formik';
import { validate } from 'gerador-validador-cpf';
import qs from 'qs';

import Bandeiras from '../../assets/bandeiras-rede.jpg';
import BB from '../../assets/bb.png';
import Boleto from '../../assets/boleto.jpg';
import Bradesco from '../../assets/bradesco.png';
import Caixa from '../../assets/caixa.png';
import Itau from '../../assets/itau.png';
import logousadofacil from '../../assets/logo-usadofacil.png';
import pix from '../../assets/pix.png';
import Safra from '../../assets/safra.png';
import Santander from '../../assets/santander.png';
import Sicoob from '../../assets/sicoob.png';
import api from '../../services/api';
import { maskTelefone } from '../../utils/functions';

function App() {
	const { idveiculo } = useParams();

	const [userIdentify, setUserIdendify] = useState(false);
	const [loading, setLoading] = useState(false);
	const [name, setName] = useState('');
	const [password, setPassword] = useState('');
	const [id, setID] = useState('');
	const [error, setError] = useState(false);
	const [errorMsg, setErrorMsg] = useState('');
	const [recovery, setRecovery] = useState(false);
	const [email, setEmail] = useState('');
	const [msgSucess, setMsgSucess] = useState(false);
	const [msgError, setMessageError] = useState(false);

	const handleAuthentication = (event) => {
		event.preventDefault();

		const dados = {
			usuario: name,
			senha: password,
		};

		const data = JSON.stringify(dados);

		const requestBody = qs.stringify({
			dados: data,
		});

		api.post('reserva_login_processa.asp', requestBody, {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
		})
			.then((response) => {
				if (response.data.sucesso) {
					formSubmit();
				} else {
					setError(true);
				}
			})
			.catch((response) => {
				console.log(response);
			});

		// window.location.replace('http://usadofacil.com.br/adminv8/Form_Logar_ASP.aspx?IDUsuario=147674')
	};

	const handleName = (event) => {
		setName(event.target.value);
	};

	const handlePassword = (event) => {
		setPassword(event.target.value);
	};
	const handleEmail = (event) => {
		setEmail(event.target.value);
	};

	const formSubmit = () => {
		setLoading(true);
		const formLogin = document.querySelector('#formlogin');
		formLogin.submit();
	};

	const handleRecoveryPassword = () => {
		const dados = {
			email,
		};

		const data = JSON.stringify(dados);

		const requestBody = qs.stringify({
			dados: data,
		});

		api.post('reserva_esquece_senha.asp', requestBody, {
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
		})
			.then((response) => {
				if (response.data.sucesso) {
					setMsgSucess(true);
					setMessageError(false);
					setTimeout(() => {
						setRecovery(false);
						setUserIdendify(true);
					}, 5000);
				} else {
					setMessageError(true);
				}
			})
			.catch((response) => {
				console.log(response);
				setMessageError(true);
			});
	};

	//   useEffect(() => {formSubmit()},[])

	if (loading) {
		return (
			<div
				style={{
					position: 'fixed',
					top: '50%',
					left: '50%',
					transform: 'translate(-50%, -50%)',
					display: 'flex',
					flexDirection: 'column',
					justifyContent: 'center',
					alignItems: 'center',
				}}
			>
				<div>
					<CircularProgress />
				</div>
				<div>
					<Typography component="div" variant="body1">
						Aguarde... a reserva está sendo processada.
					</Typography>
				</div>

				<div align="center" style={{ display: 'none' }}>
					<form
						name="formlogin"
						id="formlogin"
						method="post"
						action={`https://www.usadofacil.com.br/v6/anuncie_reserva.asp?reserva=${id}`}
					>
						<label>Usuário:</label>
						<input name="login" type="text" id="login" value={name} size="8" maxLength="10" />
						<label>Senha:</label>
						<input name="senha" type="password" value={password} id="senha" size="8" maxLength="10" />
						<input name="Submit" type="submit" id="Submit" value="ok" />
					</form>
				</div>
			</div>
		);
	}

	if (recovery) {
		return (
			<Box>
				<img src={logousadofacil} alt="Logo UsadoFácil" width="200px" />
				<Typography variant="h4" component="h4" style={{ color: 'green' }} gutterBottom>
					Recuperar sua senha
				</Typography>

				<Typography component="h6" variant="body1">
					Informe seu email de cadastro para recuperação da sua conta.
				</Typography>

				{msgSucess && (
					<Typography component="h6" variant="body1" style={{ marginTop: 20, color: 'green' }}>
						Dados enviados no seu email, confira as instruções.
					</Typography>
				)}
				{msgError && (
					<Typography component="h6" variant="body1" style={{ marginTop: 20, color: 'red' }}>
						O e-mail informado não foi encontrado.
					</Typography>
				)}

				<div style={{ maxWidth: 400 }}>
					<TextField
						variant="outlined"
						margin="normal"
						required
						fullWidth
						id="email"
						label="Informe o seu email"
						name="email"
						value={email}
						onChange={handleEmail}
						autoFocus
						error={error}
					/>
					<Button
						style={{ height: 50 }}
						id="ButtonLogin"
						key="LoginButton"
						name="BotaoDeLogin"
						type="submit"
						fullWidth
						variant="contained"
						color="primary"
						disabled={msgSucess}
						onClick={handleRecoveryPassword}
					>
						Enviar
					</Button>
				</div>
			</Box>
		);
	}

	if (userIdentify) {
		return (
			<div>
				<Box>
					<img src={logousadofacil} alt="Logo UsadoFácil" width="200px" />
					<Typography variant="h4" component="h4" style={{ color: 'green' }} gutterBottom>
						Reservar seu veículo
					</Typography>

					<Typography component="h6" variant="body1">
						Identificamos que você já possui cadastro em nosso site.
					</Typography>
					<Typography component="h6" variant="body1">
						Realize o login para acompanhar o andamento de sua reserva.
					</Typography>

					<div style={{ maxWidth: 400 }}>
						<form onSubmit={handleAuthentication}>
							<div>
								<TextField
									variant="outlined"
									margin="normal"
									required
									fullWidth
									id="login"
									label="Login"
									name="login"
									value={name}
									onChange={handleName}
									autoFocus
									error={error}
								/>

								<TextField
									variant="outlined"
									margin="normal"
									required
									fullWidth
									name="password"
									label="Senha"
									type="password"
									id="password"
									value={password}
									onChange={handlePassword}
									autoComplete="current-password"
									error={error}
									helperText={error && 'Usuário ou senha inválidos'}
								/>
								<div
									onClick={() => setRecovery(true)}
									style={{ color: 'red', marginTop: 5, marginBottom: 10, float: 'right' }}
								>
									<Typography component="h6" variant="body1" style={{ cursor: 'pointer' }}>
										Esqueceu sua senha ?
									</Typography>
								</div>
							</div>

							<div>
								<Button
									style={{ height: 50 }}
									id="ButtonLogin"
									key="LoginButton"
									name="BotaoDeLogin"
									type="submit"
									fullWidth
									variant="contained"
									color="primary"
									disabled={loading}
								>
									Entrar
								</Button>

								{loading && (
									<p
										id="loading"
										style={{
											display: 'flex',
											flexFlow: 'row wrap',
											justifyContent: 'center',
											alignItems: 'center',
										}}
									>
										Aguarde...
									</p>
								)}
							</div>
						</form>
					</div>
				</Box>
			</div>
		);
	}

	return (
		<div className="App" style={{ margin: 20 }}>
			<Box>
				<Typography variant="h4" component="h4" style={{ color: 'green' }} gutterBottom>
					Reservar seu veículo
				</Typography>

				<Typography component="h6" variant="body1">
					Faça a reserva do seu veículo e garanta a compra.
				</Typography>
			</Box>

			<Formik
				initialValues={{
					nome: '',
					cpf: '',
					telefone: '',
					data_nasc: '',
					email: '',
					banco: 'banco',
				}}
				validate={(values) => {
					const errors = {};
					// Validação CPF
					if (!validate(values.cpf)) {
						errors.cpf = 'CPF Inválido';
					}

					// Validação Nome
					if (!values.nome) {
						errors.nome = 'Insira seu nome e sobrenome';
					}

					// Validação Email
					if (!values.email) {
						if (!/[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/i.test(values.email)) {
							errors.email = 'Email inválido';
						}
					}

					if (!/^\([1-9]{2}\) (?:[2-8]|9[1-9])[0-9]{3}\-[0-9]{4}$/i.test(values.telefone)) {
						errors.telefone = 'Telefone inválido';
					}

					return errors;
				}}
				onSubmit={(values, { setSubmitting }) => {
					values.cpf = values.cpf.replace('.', '');
					values.cpf = values.cpf.replace('.', '');
					values.cpf = values.cpf.replace('-', '');

					const dados = {
						...values,
						idveiculo,
						valor_reserva: 1,
					};

					const data = JSON.stringify(dados);

					console.log(data);

					const requestBody = qs.stringify({
						dados: data,
					});

					api.post('reserva_login.asp', requestBody, {
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded',
						},
					})
						.then((response) => {
							setLoading(true);
							if (response.data.sucesso === false) {
								setErrorMsg(response.data.mensagem);
							} else if (response.data.sucesso && !response.data.conta_criada) {
								setID(response.data.id);
								setLoading(true);
								setTimeout(() => {
									setLoading(false);
									setUserIdendify(true);
								}, 3000);
							} else if (
								response.data.sucesso &&
								response.data.conta_criada &&
								response.data.login &&
								response.data.senha
							) {
								setID(response.data.id);
								setName(response.data.login);
								setPassword(response.data.senha);
								console.log(name, password);
								formSubmit();
							}
						})
						.catch((response) => {
							console.log(response);
						});
					// window.location.replace('http://usadofacil.com.br/adminv8/Form_Logar_ASP.aspx?IDUsuario=163330');
				}}
			>
				{({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
					<Form onSubmit={handleSubmit}>
						<div
							style={{
								marginTop: '30px',
								color: 'Red',
								maxWidth: 430,
								width: '90vw',
							}}
						>
							<TextField
								type="text"
								name="nome"
								onBlur={handleBlur}
								variant="outlined"
								style={{ width: '100%' }}
								label="Digite seu nome completo*"
								value={values.nome}
								onChange={handleChange}
								error={errors.nome && touched.nome && errors.nome}
							/>
							{errors.nome && touched.nome && errors.nome}
						</div>

						<div
							style={{
								marginTop: '20px',
								color: 'red',
								maxWidth: 430,
								width: '90vw',
							}}
						>
							<TextField
								type="text"
								id="email"
								label="Digite seu email*"
								name="email"
								style={{ width: '100%' }}
								onChange={handleChange}
								variant="outlined"
								onBlur={handleBlur}
								value={values.email}
								errors={errors.email && touched.email && errors.email}
							/>
							{errors.email && touched.email && errors.email}
						</div>
						<div
							style={{
								marginTop: '20px',
								color: 'Red',
								maxWidth: 430,
								width: '90vw',
							}}
						>
							<NumberFormat
								customInput={TextField}
								color="primary"
								id="cpf"
								name="cpf"
								type="tel"
								onChange={handleChange}
								label="Digite seu CPF*"
								onBlur={handleBlur}
								inputMode="decimal"
								variant="outlined"
								value={values.cpf}
								style={{ width: '100%' }}
								error={errors.cpf && touched.cpf && errors.cpf}
								format="###.###.###-##"
							/>
							{errors.cpf && touched.cpf && errors.cpf}
						</div>

						<div
							style={{
								marginTop: '20px',
								color: 'Red',
								maxWidth: 430,
								width: '90vw',
							}}
						>
							<TextField
								id="data_nasc"
								label="Data de nascimento"
								type="date"
								variant="outlined"
								style={{ width: '100%' }}
								// className={classes.textField}
								InputLabelProps={{
									shrink: true,
								}}
								onBlur={handleBlur}
								onChange={handleChange}
								value={values.data_nasc}
								errors={errors.data_nasc && touched.data_nasc && errors.data_nasc}
							/>
							{errors.data_nasc && touched.data_nasc && errors.data_nasc}
						</div>

						<div
							style={{
								marginTop: '20px',
								color: 'Red',
								maxWidth: 430,
								wid6th: '90vw',
							}}
						>
							<NumberFormat
								customInput={TextField}
								color="primary"
								id="telefone"
								name="telefone"
								onChange={handleChange}
								inputMode="decimal"
								label="Digite seu telefone*"
								onBlur={handleBlur}
								type="tel"
								variant="outlined"
								value={values.telefone}
								isAllowed={(values) => {
									const { floatValue } = values;
									return (floatValue >= -1 && floatValue <= 100000000000) || floatValue == null;
								}}
								style={{ width: '100%' }}
								format={maskTelefone}
								error={errors.telefone && touched.telefone && errors.telefone}
							/>
							{errors.telefone && touched.telefone && errors.telefone}
						</div>
						<div
							style={{
								marginTop: '20px',
								color: 'Red',
								maxWidth: 430,
								width: '90vw',
							}}
						>
							<Select
								name="banco"
								value={values.banco}
								onChange={handleChange}
								onBlur={handleBlur}
								variant="outlined"
								style={{ width: '100%' }}
								label="Selecione o seu banco"
							>
								<MenuItem value="banco">Banco preferencial de relacionamento</MenuItem>
								<MenuItem value="Banco do brasil">
									<div className="SelectBancos">
										<img src={BB} width={40} height={40} alt="banco" /> &nbsp; Banco do Brasil
									</div>
								</MenuItem>

								<MenuItem value="Caixa">
									<div className="SelectBancos">
										<img src={Caixa} width={40} height={40} alt="banco" /> &nbsp; Caixa
									</div>
								</MenuItem>

								<MenuItem value="Itau">
									<div className="SelectBancos">
										<img src={Itau} width={40} height={40} alt="banco" /> &nbsp;Itaú
									</div>
								</MenuItem>

								<MenuItem value="Bradesco">
									<div className="SelectBancos">
										<img src={Bradesco} width={40} height={40} alt="banco" />
										&nbsp;Bradesco
									</div>
								</MenuItem>

								<MenuItem value="Santander">
									<div className="SelectBancos">
										<img src={Santander} width={40} height={40} alt="banco" />
										&nbsp;Santander
									</div>
								</MenuItem>

								<MenuItem value="Sicoob">
									<div className="SelectBancos">
										<img src={Sicoob} width={40} height={40} alt="banco" />
										&nbsp;Sicoob
									</div>
								</MenuItem>

								<MenuItem value="Safra">
									<div className="SelectBancos">
										<img src={Safra} width={40} height={40} alt="banco" />
										&nbsp;Banco Safra
									</div>
								</MenuItem>

								<MenuItem value="Outro">
									<div className="SelectBancos">Outro</div>
								</MenuItem>
							</Select>

							{errors.banco && touched.banco && errors.banco}
						</div>

						<div style={{ marginTop: 20 }}>
							<Button
								style={{ backgroundColor: '#319795', width: '430px', height: 50 }}
								className="BtnEnviar"
								size="large"
								variant="contained"
								color="primary"
								type="submit"
								disabled={isSubmitting}
							>
								Realizar reserva do veículo
							</Button>
						</div>
					</Form>
				)}
			</Formik>

			<div>
				<Typography component="h4" style={{ color: 'green', marginTop: 20 }} variant="h5">
					Sistema de Reserva
				</Typography>
				<Typography component="body1" style={{ marginTop: 10 }} variant="body1">
					O sistema de reserva tem como objetivo facilitar o processo de aquisição do seu veículo garantindo
					que o veículo será reservado (24 horas).
				</Typography>
			</div>
			<div>
				<Typography component="h4" style={{ color: 'green', marginTop: 5 }} variant="h5">
					Como funciona ?
				</Typography>
				<Typography component="body1" style={{ marginTop: 10 }} variant="body1">
					Após o pagamento a loja proprietária do veículo receberá informações sobre a reserva realizada dando
					continuidade ao processo de compra.
				</Typography>
			</div>
			<div>
				<Typography component="h4" style={{ color: 'green', marginTop: 5 }} variant="h5">
					Posso desistir da compra após a reserva realizada ?
				</Typography>
				<Typography component="body1" style={{ marginTop: 5 }} variant="body1">
					Sim, em caso de desistência ou problema na negociação inicia-se o processo de reembolso em até 72
					horas úteis.
				</Typography>
			</div>
			<div>
				<Typography component="h4" style={{ color: 'green', marginTop: 5 }} variant="h5">
					Há garantia de crédito na compra após a realização da reserva ?
				</Typography>
				<Typography component="body1" style={{ marginTop: 5 }} variant="body1">
					Não, a reserva do veículo não garante aprovação de crédito.
				</Typography>
			</div>
			<Box>
				<Typography component="h4" style={{ color: 'green', marginTop: 10 }} variant="h5">
					Opções de pagamento
				</Typography>

				<Stack spacing={3} direction="row">
					<img src={pix} alt="rede" width="128" />
					<img src={Bandeiras} alt="rede" width="360" style={{ marginLeft: '-3px' }} />
					<img src={Boleto} alt="rede" width="128" style={{ marginLeft: '-5px' }} />
				</Stack>
			</Box>

			<div align="center" style={{ display: 'none' }}>
				<form
					name="formlogin"
					id="formlogin"
					method="post"
					action={`https://www.usadofacil.com.br/v6/anuncie_reserva.asp?reserva=${id}`}
				>
					<label>Usuário:</label>
					<input name="login" type="text" id="login" value={name} size="8" maxLength="10" />
					<label>Senha:</label>
					<input name="senha" type="password" value={password} id="senha" size="8" maxLength="10" />
					<input name="Submit" type="submit" id="Submit" value="ok" />
				</form>
			</div>
		</div>
	);
}

export default App;
