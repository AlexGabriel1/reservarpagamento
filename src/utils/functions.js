import { isFuture, isValid, isAfter } from 'date-fns';
import { default as dayjs } from 'dayjs';
// import { default as relativeTime } from 'dayjs/plugin/relativeTime';

export const maskTelefone = (val) => {
	let dd;
	let resto;
	let resto1;
	let resto2;

	if (val.length > 1) {
		dd = val.substring(0, 2);
		resto = val.slice(2, val.length);

		if (resto.length > 4 && resto.length < 9) {
			resto1 = resto.slice(0, 4);
			resto2 = resto.slice(4, val.length);
			return `(${dd}) ${resto1}-${resto2}`;
		}

		if (resto.length > 8) {
			resto1 = resto.slice(0, 5);
			resto2 = resto.slice(5, val.length);

			return `(${dd}) ${resto1}-${resto2}`;
		}
		return `(${dd}) ${resto}`;
	}
	return val;
};

export const formatDate = (data) => {
	const date = dayjs(data).format('DD/MM/YYYY');
	return date;
};

export const isValidAge = (data) => {
	const date = dayjs(data).format('DD/MM/YYYY');
	const year = parseInt(date.slice(6, 10));

	const actualYear = dayjs().year();
	const diffyear = actualYear - year;

	if (diffyear >= 18) {
		return false;
	}

	return true;
};

export const isValidDate = (date) => {
	const future = isFuture(new Date(date));
	const valid = isValid(new Date(date));
	const min_age = isAfter(new Date(date), new Date(1895, 6, 10));
	if (future) {
		return true;
	}
	if (!valid) {
		return true;
	}
	if (!min_age) {
		return true;
	}

	return false;
};
